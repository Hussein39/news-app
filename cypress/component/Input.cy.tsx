/**
 * Cypress - Component - Input
 */

// UI
import Input from '../../components/form/input'
import theme from '../../theme/theme'

// Styled
import { ThemeProvider } from 'styled-components'

describe('<Input />', () => {
  it('Testing Input Visibility', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Input id="firstName" name="firstName" />
      </ThemeProvider>
    )
    cy.get('input[name="firstName"]').should('be.visible')
    cy.get('#firstName').should('be.visible')
  })

  it('Testing Input', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Input id="firstName" name="firstName" />
      </ThemeProvider>
    )
    cy.get('input[name="firstName"]').type('Some Value').should('have.value', 'Some Value')
  })

  it('Testing Error Input', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Input id="firstName" name="firstName" errorMessage="Error Message" />
      </ThemeProvider>
    )
    cy.contains('Error Message').should('be.visible')
  })
})
