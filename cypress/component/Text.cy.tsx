/**
 * Cypress - Component - Text
 */

// UI
import Text from '../../components/atom/text'
import theme from '../../theme/theme'

// Styled
import { ThemeProvider } from 'styled-components'

describe('<Text />', () => {
  it('Testing Text Visibility', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Text content="Text" />
      </ThemeProvider>
    )
    cy.contains('Text').should('be.visible')
    cy.get('p').should('be.visible')
  })

  it('Testing Text Main Props', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Text content="Text" context="black" align="center" size={22} lineHeight={28} />
      </ThemeProvider>
    )
    cy.contains('Text').should('be.visible')
    cy.get('p').should('have.css', 'color', 'rgb(0, 0, 0)')
    cy.get('p').should('have.css', 'text-align', 'center')
    cy.get('p').should('have.css', 'font-size', '22px')
    cy.get('p').should('have.css', 'lineHeight', '28px')
  })
})
