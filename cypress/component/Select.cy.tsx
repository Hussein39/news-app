/**
 * Cypress - Component - Select
 */

// UI
import Select from '../../components/form/select'
import theme from '../../theme/theme'

// Styled
import { ThemeProvider } from 'styled-components'

// Types
import { Option } from '../../types/common/select'

describe('<Select />', () => {
  let options: Option[] = []

  before(() => {
    cy.fixture('languageOptions')
      .as('options')
      .then((item) => {
        options = item
      })
  })

  it('Testing Heading Visibility', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Select options={options} />
      </ThemeProvider>
    )

    cy.get('select').should('be.visible')
  })

  it('Testing Select functional', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Select options={options} />
      </ThemeProvider>
    )

    cy.get('select').select('en').should('be.value', 'en')
    cy.get('select').select('en').contains('English').should('be.value', 'en')
  })

  it('Testing Select Component in Text Mode', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Select options={options} label="Language:" isInlineLabel isText content="English" />
      </ThemeProvider>
    )

    cy.get('p').should('be.visible')
    cy.get('p').should('contain', 'Language:').and('contain', 'English')
  })
})
