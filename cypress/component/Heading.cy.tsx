/**
 * Cypress - Component - Button
 */

// UI
import Heading from '../../components/atom/heading'
import theme from '../../theme/theme'

// Styled
import { ThemeProvider } from 'styled-components'

describe('<Heading />', () => {
  it('Testing Heading Visibility', () => {
    cy.mount(
      <ThemeProvider theme={theme}>
        <Heading tag="h1" content="Heading" />
      </ThemeProvider>
    )
    cy.contains('Heading').should('be.visible')
    cy.get('h1').should('be.visible')
  })
})
