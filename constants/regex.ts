/**
 * constants - Validator
 */

const EMAIL = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
const NUMBER = /^\d*(\.\d+)?$/

// Min 1 uppercase letter.
// Min 1 lowercase letter.
// Min 1 special character.
// Min 1 number.
// Min 8 characters.
// Max 30 characters.
const PASSWORD = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,30}$/

export { EMAIL, NUMBER, PASSWORD }
