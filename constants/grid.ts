/**
 * constants - Grid
 */

const breakpoints = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 1024,
  xl: 1200,
  xxl: 1600
}

const containerWidths = {
  sm: 544,
  md: 720,
  lg: 1008,
  xl: 1152,
  xxl: 1536
}

const gutterWidth = 32

const GRID = {
  breakpoints,
  containerWidths,
  gutterWidth
}

export default GRID
