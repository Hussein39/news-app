/**
 * Constants - COLOR
 */

const COLOR = {
  black: '#32324D',
  borderContext: '#a2a2a2',
  inputBorder: 'rgb(220, 220, 228)',
  iconHoverContext: 'rgb(102, 102, 135)',
  mainBackground: 'rgb(246, 246, 249)',
  danger: '#FF3333',
  primary: '#271FE0',
  primaryHover: 'rgb(123, 121, 255)',
  white: '#ffffff',
  spinning: '#5099f4',
  success: 'rgb(47, 104, 70)'
}

export default COLOR
