/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  compiler: {
    styledComponents: true
  },
  env: {
    API_ROUTE: process.env.API_ROUTE
  }
}

module.exports = nextConfig
