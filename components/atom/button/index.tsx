/**
 * Components - Atom - Button
 */

// Style
import styled, { css } from 'styled-components'

// Types
import { IButton } from '../../../types/common/button'
import { CSSReturnType } from '../../../types/common/theme'

import EditIcon from '../../icons/editIcon'
import PlusIcon from '../../icons/plusIcon'
import RemoveIcon from '../../icons/removeIcon'
import ViewIcon from '../../icons/viewIcon'

const Button = ({
  children,
  context,
  className,
  disabled,
  hoverContext,
  icon,
  iconHoverContext,
  onClick,
  round,
  title,
  titleIcon,
  textContext,
  type
}: Partial<IButton>): JSX.Element => {
  const text = children || title
  return (
    <StyledButton
      context={context}
      className={className}
      disabled={disabled}
      hoverContext={hoverContext}
      icon={icon}
      iconHoverContext={iconHoverContext}
      onClick={onClick}
      role="button"
      round={round}
      textContext={textContext}
      type={type}
    >
      {icon ? (
        <Icon icon={icon} />
      ) : (
        <TitleIcon>
          <Icon icon={titleIcon} />
          <strong>{text}</strong>
        </TitleIcon>
      )}
    </StyledButton>
  )
}

const TitleIcon = styled.div`
  display: flex;
  padding: 0px 26px;
  justify-content: center;

  svg {
    position: relative;
    right: 12px;
    path {
      fill: #ffffff;
    }
  }
`
const Icon = ({ icon }: { icon?: string }): JSX.Element => {
  switch (icon) {
    case 'edit':
      return (
        <IconWrapper>
          <EditIcon />
        </IconWrapper>
      )
    case 'remove':
      return (
        <IconWrapper>
          <RemoveIcon />
        </IconWrapper>
      )
    case 'plus':
      return (
        <IconWrapper>
          <PlusIcon />
        </IconWrapper>
      )
    case 'view':
      return (
        <IconWrapper>
          <ViewIcon />
        </IconWrapper>
      )
    default:
      return <></>
  }
}
const IconWrapper = styled.div`
  width: 18px;
  height: 18px;
`
const StyledButton = styled.button<Partial<IButton>>`
  color: ${({ textContext, theme }): string => textContext && theme?.COLOR?.[textContext]};
  font-size: 1rem;
  min-width: 8rem;
  height: 2.5rem;
  line-height: 100%;
  border: 0;
  cursor: pointer;
  background-color: ${({ context, theme: { COLOR } }): string => context && COLOR[context]};
  border-radius: ${({ round }): string => (round ? `${round}px ${round}px` : 'unset')};

  & + & {
    margin-left: ${({ icon }: any): CSSReturnType => (icon ? '4px' : '12px')};
  }
  ${({ icon }: any): CSSReturnType =>
    icon &&
    css`
      min-width: 12px;
      height: 12px;
      padding: 8px 0px;
      margin-right: 8px;
      background: none;
      border: none;
      border-radius: none;
      transition: 0.5s all;
      &:hover {
        path {
          fill: ${({ iconHoverContext, theme }: any): CSSReturnType =>
            iconHoverContext && theme?.COLOR?.[iconHoverContext]};
        }
      }
    `};

  &:hover {
    background-color: ${({ hoverContext, theme }): CSSReturnType => hoverContext && theme.COLOR[hoverContext]};
  }
`

export default Button
