/**
 * Components - Atom - Text
 */

// Style
import styled, { css } from 'styled-components'

// Types
import { IText } from '../../../types/common/text'
import { CSSReturnType } from '../../../types/common/theme'

const Text = ({
  align,
  children,
  className,
  content,
  context,
  isInlineLabel,
  lineHeight,
  label,
  size,
  weight
}: Partial<IText>): JSX.Element => {
  return (
    <StyledText
      className={className}
      align={align}
      context={context}
      lineHeight={lineHeight}
      size={size}
      weight={weight}
    >
      {label && (
        <>
          {isInlineLabel ? (
            <strong>{label}&nbsp;&nbsp;</strong>
          ) : (
            <p>
              <strong>{label}&nbsp;&nbsp;</strong>
            </p>
          )}
        </>
      )}
      {content || children}
    </StyledText>
  )
}

const StyledText = styled.p<Partial<IText>>`
  color: ${({ context, theme }): CSSReturnType => context && theme?.COLOR?.[context]};
  margin: 0;
  padding: 0;
  text-align: ${({ align }): CSSReturnType => align};

  ${({ size, lineHeight }): CSSReturnType => css`
    font-size: ${`${size}px`};
    line-height: ${`${lineHeight}px`};
  `}

  ${({ weight }): CSSReturnType =>
    weight &&
    css`
      font-weight: ${weight === 'light'
        ? 400
        : weight === 'regular'
        ? 500
        : weight === 'semiBold'
        ? 600
        : weight === 'bold'
        ? 700
        : 500};
    `}
`

export default Text
