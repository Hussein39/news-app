/**
 * Components - Atom - Heading
 */

// React
import { createElement } from 'react'

// Style
import styled, { css } from 'styled-components'

// Types
import { IHeading } from '../../../types/common/heading'
import { CSSReturnType } from '../../../types/common/theme'

const Heading = ({ content, context, tag }: IHeading): JSX.Element => {
  return (
    <StyledHeading context={context} tag={tag}>
      {createElement(tag, { content, tag }, content)}
    </StyledHeading>
  )
}

const StyledHeading = styled.div<IHeading>`
  color: ${({ context, theme }): CSSReturnType => context && theme.COLOR[context]};
  ${({ tag, theme }): CSSReturnType => css`
    font-family: ${theme.HEADING?.[tag]?.fontFamily};
    font-size: ${theme.HEADING?.[tag]?.fontSize};
    line-height: ${theme.HEADING?.[tag]?.lineHeight};
    text-transform: ${theme.HEADING?.[tag]?.textTransform};
    margin: 0;
  `}
`

export default Heading
