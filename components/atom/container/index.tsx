/**
 * Components - Atom - Container
 */

// Style
import styled, { css } from 'styled-components'

// Types
import { ITheme } from '../../../types/common/theme'
import { CSSReturnType } from '../../../types/common/theme'

const Container = styled.div<ITheme>`
  box-sizing: border-box;
  margin-left: auto;
  margin-right: auto;
  position: relative;

  ${({ theme }): CSSReturnType => css`
    padding-left: ${theme.GRID.gutterWidth / 2}px;
    padding-right: ${theme.GRID.gutterWidth / 2}px;
  `}

  @media (min-width: ${({ theme }): CSSReturnType => theme.GRID.breakpoints.sm}px) {
    max-width: ${({ theme }): CSSReturnType => theme.GRID.containerWidths.sm}px;
  }

  @media (min-width: ${({ theme }): CSSReturnType => theme.GRID.breakpoints.md}px) {
    max-width: ${({ theme }): CSSReturnType => theme.GRID.containerWidths.md}px;
  }

  @media (min-width: ${({ theme }): CSSReturnType => theme.GRID.breakpoints.lg}px) {
    max-width: ${({ theme }): CSSReturnType => theme.GRID.containerWidths.lg}px;
  }

  @media (min-width: ${({ theme }): CSSReturnType => theme.GRID.breakpoints.xl}px) {
    max-width: ${({ theme }): CSSReturnType => theme.GRID.containerWidths.xl}px;
  }

  @media (min-width: ${({ theme }): CSSReturnType => theme.GRID.breakpoints.xxl}px) {
    max-width: ${({ theme }): CSSReturnType => theme.GRID.containerWidths.xxl}px;
  }
`

export default Container
