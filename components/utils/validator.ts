/**
 * Utils - Validator
 */

// React
import { cloneElement } from 'react'

// util
import getFormValues from './getFormValues'

// Types
import { EMAIL, NUMBER, PASSWORD } from '../../constants/regex'
import { IValidator, IValidatorReturn, ISchema, IForm, IRole } from '../../types/common/form'

export default ({ e, formElements, setFormElementProps, schema }: IValidator): IValidatorReturn => {
  const elements: JSX.Element[] = []
  let isValid = true

  const values: Partial<IForm> = getFormValues(e?.target, formElements)

  formElements.forEach((item: JSX.Element) => {
    const { roles = [], name = '' }: Partial<ISchema> = schema.find(({ name }) => item?.props?.name === name) || {}
    type Values = keyof typeof values

    const currentError = checkRoles(roles, values[name as Values])
    elements.push(cloneElement(item, { errorMessage: currentError }))
  })
  isValid = elements.every((element) => element?.props?.errorMessage === '')

  setFormElementProps(elements)
  return { values, isValid }
}

const checkRoles = (roles: IRole[], value: string): string => {
  let result = ''

  roles.forEach(({ role, message }: IRole) => {
    switch (role) {
      case 'isEmail':
        if (isEmail(value)) {
          result = message
        }
        break
      case 'isNumber':
        if (isNumber(value)) {
          result = message
        }
        break

      case 'isPassword':
        if (isPassword(value)) {
          result = message
        }
        break
      case 'isRequired':
        if (isRequire(value)) {
          result = message
        }
        break
      default:
        break
    }
  })
  return result
}

const isNumber = (value: string): boolean => !value.match(NUMBER)

const isEmail = (value: string): boolean => !value.match(EMAIL)

const isRequire = (value: string): boolean => !value

const isPassword = (value: string): boolean => !value.match(PASSWORD)
