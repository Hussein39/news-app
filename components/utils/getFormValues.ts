/**
 * Utils - Validator
 */

// Types
import { IForm } from '../../types/common/form'

const getFormValues = (target: JSX.Element, formElements: JSX.Element[]): Partial<IForm> => {
  const values: Partial<IForm> = {}
  type Target = keyof typeof target
  type Values = keyof typeof values

  formElements.forEach((item: JSX.Element) => {
    const name = item?.props?.name
    if (name) values[name as Values] = target[name as Target]?.value
  })
  return values
}

export default getFormValues
