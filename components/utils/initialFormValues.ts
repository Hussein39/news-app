/**
 *  Components - Utils - initialFormValues
 */

// React
import { cloneElement } from 'react'

const initialFormValues = (formElements: JSX.Element[], initialValues: any): JSX.Element[] => {
  const elements: JSX.Element[] = []

  formElements.forEach((item: JSX.Element) => {
    elements.push(cloneElement(item, { defaultValue: initialValues[item?.props.name] }))
  })

  return elements
}

export default initialFormValues
