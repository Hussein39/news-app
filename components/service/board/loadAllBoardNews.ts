/**
 *  Components - Service - Board - Load All Board News
 */

// Utils
import fetcher from '../../utils/fetcher'

export default async (board: string): Promise<any> => {
  return await fetcher({ method: 'GET', url: `board/${board}/news` })
}
