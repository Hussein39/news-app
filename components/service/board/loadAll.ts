/**
 *  Components - Service - Board - Load All
 */

// Utils
import fetcher from '../../utils/fetcher'

// Types
import { IBoardAPI } from '../../../types/news/board'

const loadAll = async (): Promise<IBoardAPI> => {
  return await fetcher({ method: 'GET', url: 'board' })
}

export default loadAll
