/**
 *  Components - Service - News - Update
 */

// Utils
import fetcher from '../../utils/fetcher'

const update = async (data: any): Promise<any> => {
  const user = JSON.parse(localStorage.getItem('currentUser') as any)

  return await fetcher({
    method: 'PUT',
    url: `news`,
    body: JSON.stringify({ ...data, author: user?.email })
  })
}
export default update
