/**
 *  Components - Service - News - Create
 */

// Utils
import fetcher from '../../utils/fetcher'

// Style
import { INews } from '../../../types/news/news'

const create = async (data: INews): Promise<any> => {
  type News = keyof typeof data
  const user = JSON.parse(localStorage.getItem('currentUser') as News)

  return await fetcher({
    method: 'POST',
    url: 'news',
    body: JSON.stringify({ ...data, author: user?.email })
  })
}
export default create
