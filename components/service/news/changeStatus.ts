/**
 *  Components - Service - News - Change Status
 */

// Utils
import fetcher from '../../../components/utils/fetcher'

export default (id: string, status: string): any => {
  return fetcher({ method: 'POST', url: `news/${id}/${status}` })
}
