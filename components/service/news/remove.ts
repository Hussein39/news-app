/**
 *  Components - Service - News - Delete
 */

// Utils
import fetcher from '../../../components/utils/fetcher'

export default async (id: string): Promise<any> => {
  try {
    return fetcher({ method: 'DELETE', url: `news/${id}` })
  } catch (error) {
    return {
      error
    }
  }
}
