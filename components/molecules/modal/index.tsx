/**
 * Components - Molecules - Modal
 */

// UI
import Button from '../../atom/button'
import Heading from '../../atom/heading'
import Text from '../../atom/text'

// Style
import styled, { css } from 'styled-components'

// Types
import { IModal } from '../../../types/common/modal'
import { CSSReturnType } from '../../../types/common/theme'

const Modal = ({
  children,
  isQuestion,
  isRounded,
  setShowModal,
  title,
  titleContext,
  onStatus,
  message
}: IModal): JSX.Element => {
  const closeModal = (): void => {
    if (isQuestion) {
      onStatus && onStatus(false)
      setShowModal({ visible: false, message: '', title: '' })
      return
    }
    setShowModal(false)
  }

  return (
    <StyledModal>
      <Wrapper isQuestion={isQuestion} isRounded={isRounded}>
        <>
          <Header>
            <Heading tag="h3" content={title} context={titleContext} />
            <CloseIcon onClick={closeModal} />
          </Header>
          {isQuestion ? (
            <>
              <TextWrapper context="black">{message}</TextWrapper>
              <ModalControl>
                <Button onClick={(): void => onStatus && onStatus(true)} round={5}>
                  Yes
                </Button>
                <Button onClick={(): void => onStatus && onStatus(false)} round={5}>
                  No
                </Button>
              </ModalControl>
            </>
          ) : (
            children
          )}
        </>
      </Wrapper>
    </StyledModal>
  )
}

const TextWrapper = styled(Text)`
  margin-left: 40px;
  margin-top: 24px;
`
const ModalControl = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-end;
  margin-top: 30px;
`
const StyledModal = styled.div`
  position: fixed;
  overflow-y: scroll;
  background-color: rgba(100, 100, 100, 0.8);
  right: 0px;
  top: 0px;
  height: 100%;
  width: 100%;
  z-index: 100;
  border-left: 1px solid ${({ theme }): CSSReturnType => theme.COLOR.borderContext};
  -webkit-box-shadow: -3px 5px 14px 8px rgba(0, 0, 0, 0.79);
  box-shadow: -3px 5px 14px 8px rgba(0, 0, 0, 0.79);
`

const Wrapper = styled.div<Partial<IModal>>`
  background-color: #ffffff;
  position: absolute;
  right: 0px;
  top: 0px;
  height: 100vh;
  width: 650px;
  border-radius: ${({ isRounded }): CSSReturnType => isRounded && '8px 8px'};

  ${({ isQuestion }): CSSReturnType =>
    isQuestion &&
    css`
      position: absolute;
      top: 50%;
      left: 50%;
      margin-top: -200px;
      transform: translate(-50%, -50%);
      width: 600px;
      height: 200px;
    `};
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0px 40px;
  border-bottom: 1px solid ${({ theme }): CSSReturnType => theme.COLOR.borderContext};
`

const CloseIcon = styled.a`
  position: absolute;
  right: 32px;
  top: 16px;
  width: 32px;
  height: 32px;
  opacity: 0.3;
  cursor: pointer;
  &:hover {
    opacity: 1;
  }
  &:before,
  &:after {
    position: absolute;
    left: 15px;
    content: ' ';
    height: 33px;
    width: 2px;
    background-color: #333;
  }
  &:before {
    transform: rotate(45deg);
  }
  &:after {
    transform: rotate(-45deg);
  }
`

export default Modal
