/**
 * Components - Molecules - Table - Components - MainTable
 */

// React
import { ReactNode } from 'react'

// Style
import styled from 'styled-components'

const MainTable = ({ children }: { children: ReactNode }): JSX.Element => {
  return <Wrapper>{children}</Wrapper>
}

const Wrapper = styled.div`
  display: table;
  text-align: center;
  width: 100%;
  border-collapse: separate;
  font-family: 'Roboto', sans-serif;
  font-weight: 400;
  border-collapse: collapse;
  height: 90%;

  @media screen and (max-width: 900px) {
    width: 90%;
  }

  @media screen and (max-width: 650px) {
    display: block;
  }
`
export default MainTable
