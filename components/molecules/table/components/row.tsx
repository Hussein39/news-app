/**
 * Components - Molecules - Table - Components - Row
 */

// UI
import { Column } from '../../../../types/common/table'

// Style
import styled from 'styled-components'

// Types
import { INews } from '../../../../types/news/news'

const Row = ({ item, column }: { item: INews; column: Column[] }): JSX.Element => {
  type News = keyof typeof item
  return (
    <RowWrapper>
      {column?.map((c: Column) => (
        <>
          {!c?.hidden && (
            <>
              <TableSmall>
                <TableCell>{c.label}</TableCell>
                <TableCell>{!!c?.formatter ? c?.formatter(item) : item[c?.key as News]}</TableCell>
              </TableSmall>
            </>
          )}
        </>
      ))}
    </RowWrapper>
  )
}

const RowWrapper = styled.div`
  display: table-row;
  overflow: hidden;

  @media screen and (max-width: 650px) {
    display: table;
    width: 100%;
    border-collapse: separate;
    padding-bottom: 20px;
    margin: 5% auto 0;
    text-align: center;
  }

  background: #ffffff;

  &:nth-child(2n + 3) {
    background: #fafafa !important;

    @media screen and (max-width: 650px) {
      background: none;
      &:last-child {
        > div {
          &:last-child {
            > div {
              &:last-child {
                border-top: #ccc 1px solid;
                border-bottom-right-radius: 5px;
              }
            }
          }
          &:first-child {
            > div {
              &:last-child {
                border-top: #ccc 1px solid;
                border-bottom-left-radius: 5px;
              }
            }
          }
        }
      }
    }
  }
`

const TableSmall = styled.div`
  display: table-cell;
  vertical-align: middle;
  line-height: normal;

  @media screen and (max-width: 650px) {
    display: table-row;

    &:first-child {
      > div {
        border-left: #ccc 1px solid;
      }
    }
  }
`

const TableCell = styled.div`
  color: #5b5b5b;

  @media screen and (max-width: 650px) {
    border-bottom: #ccc 1px solid;
    display: table-cell;
    width: 50%;
  }

  &:nth-child(odd) {
    display: none;
    background: #bdbdbd;
    color: #e5e5e5;
    padding-top: 10px;
    padding-bottom: 10px;

    @media screen and (max-width: 650px) {
      display: table-cell;
      width: 50%;
      vertical-align: middle;
    }
  }
`

export default Row
