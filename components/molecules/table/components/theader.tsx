/**
 * Components - Molecules - Table - Components - TableHeader
 */

// UI
import { Column } from '../../../../types/common/table'

// Style
import styled from 'styled-components'

const TableHeader = ({ column }: { column: Column[] }): JSX.Element => {
  return (
    <Wrapper>{column?.map((item: Column) => !item?.hidden && <Header key={item?.key}>{item?.label}</Header>)}</Wrapper>
  )
}

const Header = styled.div`
  display: table-cell;
  border-bottom: #e2e2e2 1px solid;
  background: #ffffff;
  height: 48px;
  color: black;
  padding-top: 16px;
  padding-bottom: 10px;
  font-weight: 700;

  &:first-child {
    border-top-left-radius: 8px;
  }

  &:last-child {
    border-top-right-radius: 8px;
  }
`
const Wrapper = styled.div`
  display: table-row;

  @media screen and (max-width: 650px) {
    display: none;
  }
`
export default TableHeader
