/**
 * Components - Molecules - Table
 */

// UI
import MainTable from './components/MainTable'
import TableHeader from './components/theader'
import Row from './components/row'

// Style
import styled from 'styled-components'

// Types
import { ITable } from '../../../types/common/table'

const Table = ({ column = [], dataSource = [] }: ITable): JSX.Element => (
  <StyledWrapper>
    <MainTable>
      <TableHeader column={column} />
      {dataSource.map((item: any) => {
        return <Row item={item} column={column} />
      })}
    </MainTable>
  </StyledWrapper>
)

const StyledWrapper = styled.div`
  .table_row {
    display: table-row;
  }

  .table_small {
    display: table-cell;
  }

  .table_row > .table_small > .table_cell:nth-child(odd) {
    display: none;
    background: #bdbdbd;
    color: #e5e5e5;
    padding-top: 10px;
    padding-bottom: 10px;
  }

  .table_row > .table_small > .table_cell {
    padding-top: 3px;
    padding-bottom: 3px;
    color: #5b5b5b;
    border-bottom: #ccc 1px solid;
  }

  .table_row > .table_small:first-child > .table_cell {
    border-left: #ccc 1px solid;
  }

  .table_row > .table_small:last-child > .table_cell {
    border-right: #ccc 1px solid;
  }

  .table_row:last-child > .table_small:last-child > .table_cell:last-child {
    border-bottom-right-radius: 5px;
  }

  .table_row:last-child > .table_small:first-child > .table_cell:last-child {
    border-bottom-left-radius: 5px;
  }

  .table_row:nth-child(2n + 3) {
    background: #e9e9e9;
  }

  @media screen and (max-width: 650px) {
    .table_row:nth-child(2n + 3) {
      background: none;
    }

    .table_row > .table_small > .table_cell:nth-child(odd) {
      display: table-cell;
      width: 50%;
    }
    .table_cell {
      display: table-cell;
      width: 50%;
    }
    .table_row {
      display: table;
      width: 100%;
      border-collapse: separate;
      padding-bottom: 20px;
      margin: 5% auto 0;
      text-align: center;
    }
    .table_small {
      display: table-row;
    }
    .table_row > .table_small:first-child > .table_cell:last-child {
      border-left: none;
    }
    .table_row > .table_small > .table_cell:first-child {
      border-left: #ccc 1px solid;
    }
    .table_row > .table_small:first-child > .table_cell:first-child {
      border-top-left-radius: 5px;
      border-top: #ccc 1px solid;
    }
    .table_row > .table_small:first-child > .table_cell:last-child {
      border-top-right-radius: 5px;
      border-top: #ccc 1px solid;
    }
    .table_row > .table_small:last-child > .table_cell:first-child {
      border-right: none;
    }
    .table_row > .table_small > .table_cell:last-child {
      border-right: #ccc 1px solid;
    }
    .table_row > .table_small:last-child > .table_cell:first-child {
      border-bottom-left-radius: 5px;
    }
    .table_row > .table_small:last-child > .table_cell:last-child {
      border-bottom-right-radius: 5px;
    }
  }
`

export default Table
