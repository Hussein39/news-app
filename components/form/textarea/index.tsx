/**
 * Components - Form - Input
 */

// UI
import Text from '../../atom/text'

// Style
import styled from 'styled-components'

// Types
import { IInput } from '../../../types/common/input'
import { CSSReturnType } from '../../../types/common/theme'

const Textarea = ({
  autoFocus,
  borderContext,
  context,
  className,
  disabled,
  defaultValue,
  errorMessage,
  id,
  label,
  name,
  placeholder,
  type,
  textContext,
  value
}: Partial<IInput>): JSX.Element => {
  return (
    <InputWrapper className={className}>
      {label && (
        <Title>
          <strong>{label}&nbsp;&nbsp;</strong>
        </Title>
      )}
      <TextInput borderContext={borderContext} errorMessage={errorMessage}>
        <StyledInput
          context={context}
          disabled={disabled}
          defaultValue={defaultValue}
          id={id}
          name={name}
          placeholder={placeholder}
          type={type}
          textContext={textContext}
          value={value}
          autoFocus={autoFocus}
        />
      </TextInput>
      {errorMessage && <ErrorMessage content={errorMessage} context="danger" size={12} align="left" />}
    </InputWrapper>
  )
}

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const TextInput = styled.div<Partial<IInput>>`
  width: 87%;
  overflow: hidden;
  margin: 0px auto;
  border-radius: 5px 5px;
  border: 1px solid
    ${({ borderContext, errorMessage, theme }): CSSReturnType =>
      borderContext && theme?.COLOR?.[!!errorMessage ? 'danger' : borderContext]};

  p {
    position: absolute;
    bottom: 0px;
  }
`

const Title = styled.p`
  font-size: 14px;
  top: 6px;
  margin-left: 44px;

  background-color: #ffffff;
  height: 10px;
  z-index: 10;
`

const StyledInput = styled.textarea<Partial<IInput>>`
  appearance: none;
  outline: 0;
  border: 0;
  box-shadow: none;
  padding: 6px 1em;
  width: 100%;
  color: ${({ textContext, theme }): CSSReturnType => textContext && theme?.COLOR?.[textContext]};
  background-color: ${({ theme, context }): CSSReturnType => context && theme?.COLOR?.[context]};
  background-image: none;
  margin-left: 4px;
  min-height: 300px;
  display: inline;
`

const ErrorMessage = styled(Text)`
  margin-top: 8px;
  margin-left: 44px;
`

export default Textarea
