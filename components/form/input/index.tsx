/**
 * Components - Form - Input
 */

// UI
import Text from '../../atom/text'

// Style
import styled from 'styled-components'

// Types
import { IInput } from '../../../types/common/input'
import { CSSReturnType } from '../../../types/common/theme'

const Input = ({
  autoFocus,
  borderContext,
  context,
  disabled,
  defaultValue,
  errorMessage,
  id,
  label,
  name,
  onChange,
  placeholder,
  type,
  textContext,
  value
}: Partial<IInput>): JSX.Element => {
  return (
    <InputWrapper>
      <TextInput borderContext={borderContext} errorMessage={errorMessage}>
        {label && (
          <Title>
            <strong>{label}&nbsp;&nbsp;</strong>
          </Title>
        )}
        <StyledInput
          context={context}
          disabled={disabled}
          defaultValue={defaultValue}
          id={id}
          name={name}
          onChange={onChange}
          placeholder={placeholder}
          type={type}
          textContext={textContext}
          value={value}
          autoFocus={autoFocus}
        />
      </TextInput>
      {errorMessage && <ErrorMessage content={errorMessage} context="danger" size={12} align="left" />}
    </InputWrapper>
  )
}

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const TextInput = styled.div<Partial<IInput>>`
  position: relative;
  display: flex;
  width: 87%;
  margin: 12px auto;

  height: 3.2em;
  top: 2px;
  border-radius: 0.25em;
  overflow: hidden;

  border: 1px solid
    ${({ borderContext, errorMessage, theme }): CSSReturnType =>
      borderContext && theme?.COLOR?.[!!errorMessage ? 'danger' : borderContext]};

  p {
    position: absolute;
    bottom: 0px;
  }
`

const Title = styled.p`
  font-size: 12px;
  position: absolute;
  top: 6px;
  background-color: #ffffff;
  height: 10px;
  z-index: 10;
  left: 8px;
`

const StyledInput = styled.input<Partial<IInput>>`
  appearance: none;
  outline: 0;
  border: 0;
  box-shadow: none;
  padding: 0 1em;
  width: 100%;
  color: ${({ textContext, theme }): CSSReturnType => textContext && theme?.COLOR?.[textContext]};
  background-color: ${({ theme, context }): CSSReturnType => context && theme?.COLOR?.[context]};
  background-image: none;
  margin-left: 70px;
  display: inline;
`

const ErrorMessage = styled(Text)`
  margin-top: 0px;
  margin-left: 40px;
`

export default Input
