/**
 *  Components - Form - Form
 */

// React
import { useState } from 'react'

// UI
import validator from '../../utils/validator'
import Text from '../../atom/text'
import Spinning from '../../atom/spinning'
import initialFormValues from '../../utils/initialFormValues'

// Style
import styled from 'styled-components'

// Types
import { IForm } from '../../../types/common/form'
import Button from '../../atom/button'

const Form = ({
  status,
  children,
  className,
  handleSubmit,
  id,
  initialValues,
  isUpdate,
  isView,
  schema,
  loading
}: IForm): JSX.Element => {
  let childElements = children

  // initialValues
  if (Object.keys(initialValues || {}).length && childElements) {
    childElements = initialFormValues(childElements, initialValues)
  }

  const [formElements, setFormElementProps] = useState(childElements)

  const onSubmit = (e: React.FormEvent<HTMLInputElement>): boolean => {
    e.preventDefault()

    const { isValid, values } = validator({ e, formElements, setFormElementProps, schema })

    if (isValid) {
      handleSubmit(values)
      return false
    }
    return false
  }

  return (
    <StyledForm className={className} id={id} onSubmit={onSubmit}>
      <> {formElements}</>
      <Message>
        {status?.error && <Text context="danger">{status?.message}</Text>}
        {status?.status === 200 && <Text context="success">{status?.message}</Text>}
      </Message>
      {!isView ? (
        <ButtonWrapper>
          <Button disabled={loading} hoverContext="primaryHover" context="primary" textContext="white" round={5}>
            {loading && <Spinning />}
            <strong>{isUpdate ? 'Update' : 'Submit'}</strong>
          </Button>
        </ButtonWrapper>
      ) : (
        <></>
      )}
    </StyledForm>
  )
}

const StyledForm = styled.form<Partial<IForm>>``
const Message = styled.div`
  margin: 24px;
`
const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;
  margin-right: 24px;
`
export default Form
