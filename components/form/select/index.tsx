/**
 * Components - Form - Select
 */

// UI
import Text from '../../atom/text'

// Style
import styled, { css } from 'styled-components'

// Types
import { CSSReturnType } from '../../../types/common/theme'
import { ISelect, Option } from '../../../types/common/select'

const Select = ({
  arrowBorderContext,
  arrowContext,
  arrowHoverContext,
  borderContext,
  context,
  content,
  className,
  defaultValue,
  disabled,
  isInlineLabel,
  isText,
  label,
  name,
  options,
  onChange,
  textContext
}: Partial<ISelect>): JSX.Element => {
  return !isText ? (
    <Wrapper isInlineLabel={isInlineLabel}>
      {label && <Label htmlFor={name}>{label}</Label>}
      <StyledSelect
        arrowContext={arrowContext}
        arrowHoverContext={arrowHoverContext}
        arrowBorderContext={arrowBorderContext}
        borderContext={borderContext}
        context={context}
        className={className}
        textContext={textContext}
      >
        <select
          disabled={disabled}
          name={name}
          id={name}
          defaultValue={defaultValue}
          onChange={({ target }): void => onChange && onChange(target?.value)}
        >
          <option value="DEFAULT" disabled>
            Select Options
          </option>
          {(options || []).map(({ id, name }: Option) => (
            <option value={id} key={id}>
              {name}
            </option>
          ))}
        </select>
      </StyledSelect>
    </Wrapper>
  ) : (
    <Text label={label} isInlineLabel={isInlineLabel} context={context}>
      {content}
    </Text>
  )
}

const Wrapper = styled.div<Partial<ISelect>>`
  ${({ isInlineLabel }): CSSReturnType =>
    isInlineLabel &&
    css`
      align-items: center;
      display: flex;
    `}
`

const Label = styled.label`
  padding-right: 10px;
  display: inline-block;
  margin-bottom: 0px;
  font-weight: 500;
  font-size: 14px;
  margin-top: 12px;
  margin-left: 42px;
`

const StyledSelect = styled.div<Partial<ISelect>>`
  position: relative;
  display: flex;
  width: 87%;
  margin: 12px auto;
  height: 3em;
  border-radius: 0.25em;
  overflow: hidden;
  cursor: pointer;
  border: 1px solid
    ${({ borderContext, theme }): CSSReturnType => (borderContext ? theme?.COLOR?.[borderContext] : borderContext)};

  &:after {
    content: '\\25BC';
    position: absolute;
    top: 0;
    right: 0;
    padding: 1em;
    color: ${({ borderContext, theme }): CSSReturnType =>
      borderContext ? theme?.COLOR?.[borderContext] : borderContext};
    border-left: 1px solid
      ${({ borderContext, theme }): CSSReturnType => (borderContext ? theme?.COLOR?.[borderContext] : borderContext)};
    background-color: ${({ arrowContext, theme }): CSSReturnType =>
      arrowContext ? theme?.COLOR?.[arrowContext] : arrowContext};
    transition: 0.25s all ease;
    pointer-events: none;
  }

  &:hover {
    &:after {
      color: ${({ arrowHoverContext, theme }): CSSReturnType =>
        arrowHoverContext ? theme?.COLOR?.[arrowHoverContext] : arrowHoverContext} !important;
    }
  }

  > select {
    appearance: none;
    outline: 0;
    border: 0;
    box-shadow: none;
    flex: 1;
    padding: 0 1em;
    color: ${({ textContext, theme }): CSSReturnType => (textContext ? theme?.COLOR?.[textContext] : textContext)};
    background-color: ${({ context, theme }): CSSReturnType => (context ? theme?.COLOR?.[context] : context)};
    background-image: none;
    cursor: pointer;
    transition: all 0.5s;

    option {
      padding: 0px 10px;
    }
  }
`

export default Select
