/**
 * Components - Pages - Home - Components - DeleteItemModal
 */

// UI
import Modal from '../../../molecules/modal'

// Types
import { INewsTableItem } from '../../../../types/news/newsTable'

const DeleteItemModal = ({ dialog, handleStatus, setDialog }: INewsTableItem): JSX.Element => (
  <>
    {dialog?.visible ? (
      <Modal
        isQuestion
        title={dialog?.title}
        message={dialog?.message}
        setShowModal={setDialog}
        onStatus={handleStatus}
      />
    ) : (
      <></>
    )}
  </>
)

export default DeleteItemModal
