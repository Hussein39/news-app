/**
 * Components - Pages - Home - Components - newsForm
 */

// React
import { useState } from 'react'

// UI
import Form from '../../../form/form'
import Input from '../../../form/input'
import Select from '../../../form/select'
import Textarea from '../../../form/textarea'

// Schema
import newsSchema from './newsSchema'

// API
import createNews from '../../../service/news/create'
import updateNews from '../../../service/news/update'

// Types
import { INewsForm } from '../../../../types/news/newsForm'
import { INews } from '../../../../types/news/news'

const NewsForm = ({ boards, onRefresh, isUpdate, initialValues, isView }: Partial<INewsForm>): JSX.Element => {
  const [status, setStatus] = useState({ loading: false })
  const [loading, setLoading] = useState(false)

  const onSubmit = async (data: INews): Promise<void> => {
    setLoading(true)
    const result = await (isUpdate ? updateNews(data) : createNews(data))
    const message = `Successfully ${isUpdate ? 'Updated' : 'Created'}!`
    setStatus({ message: result?.status === 200 && message, ...result })
    setLoading(false)
    onRefresh && onRefresh()
  }

  return (
    <>
      <Form
        isView={isView}
        isUpdate={isUpdate}
        handleSubmit={onSubmit}
        schema={newsSchema}
        status={status}
        loading={loading}
        initialValues={initialValues}
      >
        <Input label="News Title:" name="title" autoFocus borderContext="inputBorder" disabled={isView} />
        <Input label="Image URL:" name="imageUrl" borderContext="inputBorder" disabled={isView} />

        <Select
          arrowBorderContext="inputBorder"
          arrowContext="white"
          arrowHoverContext="black"
          borderContext="inputBorder"
          context="white"
          disabled={isView}
          label="Status:"
          options={[
            { name: 'Archive', id: 'archive' },
            { name: 'Draft', id: 'draft' },
            { name: 'Published', id: 'published' }
          ]}
          name="status"
        />
        <Select
          arrowBorderContext="inputBorder"
          arrowContext="white"
          arrowHoverContext="black"
          borderContext="inputBorder"
          context="white"
          disabled={isView}
          name="boardId"
          label="Board:"
          options={boards?.data}
        />

        <Textarea borderContext="inputBorder" disabled={isView} label="Description:" name="description" />

        <input type="hidden" name="id" />
      </Form>
    </>
  )
}

export default NewsForm
