/**
 * Components - Pages - News - Schema
 */

const newsSchema = [
  {
    name: 'title',
    roles: [{ role: 'isRequired', message: 'Title is Required!' }]
  },
  {
    name: 'status',
    roles: [{ role: 'isRequired', message: 'Status is Required!' }]
  },
  {
    name: 'boardId',
    roles: [{ role: 'isRequired', message: 'Board is Required!' }]
  },
  {
    name: 'description',
    roles: [{ role: 'isRequired', message: 'description is Required!' }]
  },
  {
    name: 'imageUrl',
    roles: [{ role: 'isRequired', message: 'imageUrl is Required!' }]
  }
]

export default newsSchema
