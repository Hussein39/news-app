/**
 * Components - Pages - Home - Components - NewsTable
 */

// UI
import Button from '../../../atom/button'
import { Column } from '../../../../types/common/table'
import Text from '../../../atom/text'
import Select from '../../../form/select'
import Table from '../../../molecules/table'

// Style
import styled, { css } from 'styled-components'

// Types
import { CSSReturnType } from '../../../../types/common/theme'
import { INews } from '../../../../types/news/news'
import { INewsTable } from '../../../../types/news/newsTable'

const NewsTable = ({
  boardNewsAdapter,
  boards,
  handleChange,
  handleRemove,
  handleUpdate,
  handleView
}: Partial<INewsTable>): JSX.Element => {
  const COLUMN: Column[] = [
    {
      key: 'id',
      hidden: true
    },

    { key: 'title', label: 'Title' },
    {
      key: 'image',
      label: 'Image',
      formatter: (data: INews): JSX.Element => <Image src={data?.imageURL || '/logo.jpeg'} alt={data.title} />
    },
    { key: 'author', label: 'Author' },
    {
      key: 'status',
      label: 'Status',
      formatter: ({ status }: INews) => <StatusLabel status={status}>{status}</StatusLabel>
    },
    {
      key: 'action',
      label: 'Action',
      formatter: (data: INews): JSX.Element => {
        return (
          <>
            <Button
              iconHoverContext="iconHoverContext"
              icon="remove"
              onClick={(): void => handleRemove && handleRemove(data)}
            />
            <Button
              iconHoverContext="iconHoverContext"
              icon="edit"
              onClick={(): any => handleUpdate && handleUpdate(data)}
            />
            <Button
              iconHoverContext="iconHoverContext"
              icon="view"
              onClick={(): any => handleView && handleView(data)}
            />
          </>
        )
      }
    }
  ]
  const StatusLabel = styled.strong<Partial<INews>>`
    padding: 5px 10px;
    background-color: rgb(234, 245, 255);
    color: rgb(0, 96, 150);
    ${({ status, theme }: any): CSSReturnType =>
      status === 'published' &&
      css`
        background-color: rgb(234, 251, 231);

        color: ${theme.COLOR.success};
      `}
  `
  return (
    <>
      <Text>Select Board:</Text>
      <SelectWrapper
        arrowBorderContext="inputBorder"
        arrowContext="white"
        arrowHoverContext="black"
        borderContext="inputBorder"
        context="white"
        options={boards?.data}
        defaultValue="en"
        onChange={handleChange}
      />
      <Table dataSource={boardNewsAdapter} column={COLUMN} />
    </>
  )
}

const SelectWrapper = styled(Select)`
  margin: 12px 0px;
  width: 40%;
`

const Image = styled.img`
  max-width: 40px;
  max-height: 40px;
  border-radius: 50% 50%;
`
export default NewsTable
