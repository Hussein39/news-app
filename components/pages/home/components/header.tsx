/**
 * Components - Pages - Home - Components - Header
 */

// React
import { useState } from 'react'

// UI
import Button from '../../../atom/button'
import Heading from '../../../atom/heading'
import Modal from '../../../molecules/modal'
import Text from '../../../atom/text'
import NewsForm from './newsForm'

// Style
import styled from 'styled-components'

// Types
import { IHeader } from '../../../../types/common/header'

const Header = ({ boards, handleRefresh, searchResult }: IHeader): JSX.Element => {
  const [isShowModal, setShowModal] = useState(false)
  const handleClick = (): void => setShowModal(true)

  return (
    <Wrapper>
      <Title>
        {isShowModal && (
          <Modal title="Create News" setShowModal={setShowModal} titleContext="black">
            <NewsForm boards={boards} onRefresh={handleRefresh} />
          </Modal>
        )}
        <Heading content="HomePage - News Boards" tag="h2" context="black" />
        <ButtonWrapper
          titleIcon="plus"
          title="Create News"
          round={5}
          onClick={handleClick}
          hoverContext="primaryHover"
          context="primary"
          textContext="white"
        />
      </Title>
      <SearchResult align="left" size={16} lineHeight={10} context="rgb(102, 102, 135)">
        {searchResult || 0} entries Found
      </SearchResult>
    </Wrapper>
  )
}

const Wrapper = styled.div``

const Title = styled.div`
  display: flex;
  justify-content: space-between;
`

const SearchResult = styled(Text)`
  position: relative;
  top: -26px;
`
const ButtonWrapper = styled(Button)`
  margin-top: 36px;
`
export default Header
