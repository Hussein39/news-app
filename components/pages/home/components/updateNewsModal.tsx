/**
 * Components - Pages - Home - Components - Update News Modal
 */

// UI
import Button from '../../../atom/button'
import Modal from '../../../molecules/modal'
import NewsForm from './newsForm'

// API
import changeStatus from '../../../service/news/changeStatus'

// Style
import styled from 'styled-components'
import { INewsTable } from '../../../../types/news/newsTable'

const UpdateNewsModal = ({
  boards,
  initialValues,
  isView,
  setShowModal,
  isShowModal,
  handleRefresh
}: Partial<INewsTable>): JSX.Element => {
  const onChangeStatus = (status: string): any => {
    const newsId: string = initialValues?.id || ''

    switch (status) {
      case 'draft':
        changeStatus(newsId, 'draft')
        setShowModal(false)

        break
      case 'published':
        changeStatus(newsId, 'published')
        setShowModal(false)
        break
      case 'archive':
        changeStatus(newsId, 'archive')
        setShowModal(false)
        break
      default:
        break
    }
    handleRefresh && handleRefresh()
  }

  return (
    <>
      {isShowModal ? (
        <Modal title="Update News" setShowModal={setShowModal} titleContext="black">
          <>
            <NewsForm
              boards={boards}
              initialValues={initialValues}
              isUpdate
              onRefresh={handleRefresh}
              isView={isView}
            />
            {isView && (
              <ViewControl>
                <Button
                  hoverContext="primaryHover"
                  context="primary"
                  disabled={initialValues?.status === 'archive'}
                  textContext="white"
                  round={5}
                  onClick={(): void => onChangeStatus('published')}
                >
                  <strong>Publish</strong>
                </Button>
                <Button
                  hoverContext="primaryHover"
                  context="primary"
                  disabled={initialValues?.status === 'archive'}
                  textContext="white"
                  round={5}
                  onClick={(): void => onChangeStatus('draft')}
                >
                  <strong>Draft</strong>
                </Button>
                <Button
                  hoverContext="primaryHover"
                  context="primary"
                  textContext="white"
                  round={5}
                  onClick={(): void => onChangeStatus('archive')}
                >
                  <strong>Archive</strong>
                </Button>
              </ViewControl>
            )}
          </>
        </Modal>
      ) : (
        <></>
      )}
    </>
  )
}

const ViewControl = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-right: 40px;
`

export default UpdateNewsModal
