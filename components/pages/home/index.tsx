/**
 * Components - Pages - Home - View
 */

// React
import { useState } from 'react'

// Next
import { useRouter } from 'next/router'

// UI
import DeleteItemModal from './components/deleteItemModal'
import Header from './components/header'
import Layout from '../../organisms/layout/layout'
import UpdateNewsModal from './components/updateNewsModal'
import NewsTable from './components/newsTable'

// Utils
import removeNews from '../../service/news/remove'

// Type
import { IBoardNews, INews } from '../../../types/news/news'
import { IHomePage } from '../../../types/page/homePage'

const View = ({ boards, boardNews }: IHomePage): JSX.Element => {
  const { push, query } = useRouter()
  const boardNewsAdapter: IBoardNews[] = Object.values(boardNews?.data).flat()

  // TODO: should use useReducer!!
  const [isShowModal, setShowModal] = useState(false)
  const [initialValues, setInitialValues] = useState<INews>()
  const [isView, setView] = useState(false)
  const [dialog, setDialog] = useState({ visible: false, message: '', title: '' })

  const handleUpdate = (data: INews): void => {
    setShowModal(true)
    setInitialValues({ ...data })
    setView(false)
  }

  const handleView = (data: INews): void => {
    setShowModal(true)
    setInitialValues({ ...data })
    setView(true)
  }

  const handleRemove = (data: INews): void => {
    setDialog({ visible: true, title: 'Remove News', message: 'Do you want to delete this item?' })
    setInitialValues({ ...data })
  }

  const handleChange = (id: string): void => {
    push('/home?board=' + id)
  }

  const handleRefresh = (): void => {
    const { board = '' } = query
    push('/home?board=' + board)
  }

  const handleStatus = (status: boolean): void => {
    if (status) {
      removeNews(initialValues?.id || '')
      handleRefresh && handleRefresh()
    }

    setDialog({ visible: false, message: '', title: '' })
  }
  return (
    <Layout>
      <Header boards={boards} handleRefresh={handleRefresh} searchResult={boardNewsAdapter?.length} />

      <UpdateNewsModal
        boards={boards}
        initialValues={initialValues}
        isShowModal={isShowModal}
        setShowModal={setShowModal}
        handleRefresh={handleRefresh}
        isView={isView}
      />

      <DeleteItemModal dialog={dialog} setDialog={setDialog} handleStatus={handleStatus} />
      <NewsTable
        boardNewsAdapter={boardNewsAdapter}
        boards={boards}
        handleChange={handleChange}
        handleRemove={handleRemove}
        handleUpdate={handleUpdate}
        handleView={handleView}
      />
    </Layout>
  )
}

export default View
