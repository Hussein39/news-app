/**
 * Components - Pages - Login
 */

// React
import { useState } from 'react'

// UI
import Button from '../../atom/button'
import Heading from '../../atom/heading'
import Modal from '../../molecules/modal'
import Register from './components/register/register'
import SignIn from './components/login'

// Style
import styled from 'styled-components'

export default (): JSX.Element => {
  const [isShowModal, setShowModal] = useState(false)
  return (
    <>
      <Wrapper>
        <TitleWrapper>
          <Heading tag="h3" content="Login page" />
          <Button onClick={(): any => setShowModal(true)}>Register</Button>
        </TitleWrapper>
        <SignIn />
      </Wrapper>
      {isShowModal && (
        <Modal titleContext="black" setShowModal={setShowModal} title="Register">
          <Register onClose={(): any => setShowModal(false)} />
        </Modal>
      )}
    </>
  )
}

const TitleWrapper = styled.div`
  margin: 0px 36px;
  display: flex;
  justify-content: space-between;
  align-items: baseline;
`
const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;

  margin-top: -200px;
  transform: translate(-50%, -50%);
  width: 600px;
  /* height: 200px; */
  background-color: #ffffff;
  padding: 12px 42px;
  border-radius: 8px 8px;
`
