/**
 * Components - Pages - Login
 */

// Next
import { useRouter } from 'next/router'

// UI
import Form from '../../../../form/form'
import Input from '../../../../form/input'
import schema from './schema'

// Types
import { IUser } from '../../../../../types/user/user'

const SignIn = (): JSX.Element => {
  const { push } = useRouter()

  const onSubmit = (data: IUser): void => {
    const isEmailExists = localStorage.getItem(data.email)

    if (isEmailExists) {
      const result = JSON.parse(isEmailExists)

      if (result?.password === data.password) {
        localStorage.setItem('currentUser', JSON.stringify(result))
        push('/home')
      } else {
        console.log('Not Login')
      }
    } else {
      console.log('Not Exists')
    }
  }
  return (
    <>
      <Form handleSubmit={onSubmit} schema={schema}>
        <Input label="Email:" type="email" name="email" />
        <Input label="Password:" type="password" name="password" />
      </Form>
    </>
  )
}

export default SignIn
