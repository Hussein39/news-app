/**
 * Components - Pages - Auth - Login - Schema
 */

const schema = [
  {
    name: 'email',
    roles: [
      { role: 'isRequired', message: 'Email is Required!' },
      { role: 'isEmail', message: 'Email is not Valid!' }
    ]
  },
  {
    name: 'password',
    roles: [
      { role: 'isRequired', message: 'password is Required!' },
      { role: 'isPassword', message: 'password is not Valid!' }
    ]
  },
  {
    name: 'age',
    roles: [
      { role: 'isRequired', message: 'age is Required!' },
      { role: 'isNumber', message: 'age is not Valid!' }
    ]
  }
]

export default schema
