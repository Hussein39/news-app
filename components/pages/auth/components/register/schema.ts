/**
 * Components - Pages - Auth - Register - Schema
 */

const schema = [
  {
    name: 'firstName',
    roles: [{ role: 'isRequired', message: 'First Name is Required!' }]
  },
  {
    name: 'lastName',
    roles: [{ role: 'isRequired', message: 'Last Name is Required!' }]
  },
  {
    name: 'email',
    roles: [
      { role: 'isRequired', message: 'Email is Required!' },
      { role: 'isEmail', message: 'Email is not Valid!' }
    ]
  },
  {
    name: 'password',
    roles: [
      { role: 'isRequired', message: 'password is Required!' },
      { role: 'isPassword', message: 'password is not Valid!' }
    ]
  },
  {
    name: 'userType',
    roles: [{ role: 'isRequired', message: 'password is Required!' }]
  }
]

export default schema
