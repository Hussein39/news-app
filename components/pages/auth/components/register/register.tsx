/**
 * Components - Pages - Login
 */

// UI
import Form from '../../../../form/form'
import Input from '../../../../form/input'
import Select from '../../../../form/select'
import schema from './schema'

const Register = ({ onClose }: { onClose: () => void }): JSX.Element => {
  const onSubmit = (data: any): void => {
    if (localStorage.getItem(data.email)) {
      console.log('can not register')
    } else {
      localStorage.setItem(data.email, JSON.stringify(data))
      onClose()
    }
  }
  return (
    <>
      <Form handleSubmit={onSubmit} schema={schema}>
        <Input label="First Name:" type="text" name="firstName" />
        <Input label="Last Name:" type="text" name="lastName" />
        <Input label="Email:" type="email" name="email" />
        <Input label="Password:" type="password" name="password" />
        <Select
          arrowBorderContext="inputBorder"
          arrowContext="white"
          arrowHoverContext="black"
          borderContext="inputBorder"
          context="white"
          label="User type:"
          name="userType"
          options={[
            { name: 'Author', id: 'author' },
            { name: 'Editor', id: 'editor' }
          ]}
        />
      </Form>
    </>
  )
}

export default Register
