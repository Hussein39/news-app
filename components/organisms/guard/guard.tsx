/**
 *  Components - Organisms - Guard
 */

// React
import { useEffect } from 'react'

// Next
import { useRouter } from 'next/router'

const Guard = ({ children }: { children: JSX.Element }): JSX.Element => {
  const { push } = useRouter()
  useEffect(() => {
    if (localStorage.getItem('currentUser')) {
      push('/home')
    } else {
      push('/login')
    }
  }, [])
  return <>{children}</>
}

export default Guard
