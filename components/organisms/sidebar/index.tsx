/**
 *  Components - Organisms - Sidebar
 */

// Styled
import styled from 'styled-components'

const Sidebar = ({ data }: any): JSX.Element => {
  return (
    <Wrapper>
      <img src="/logo.jpeg" alt="Dashboard" width={300} height={300} />
      {data.map((item: any, index: number) => (
        <Item key={index} href={item?.href} onClick={item?.onClick}>
          {item?.title}
        </Item>
      ))}
    </Wrapper>
  )
}

const Wrapper = styled.div`
  width: 300px;
  min-height: 100%;
  border-right: 1px solid ${({ theme }): any => theme.COLOR.borderContext};
`

const Item = styled.a`
  display: block;
  text-align: center;
  transition: 0.5ms all;
  padding: 10px 0px;

  &:hover {
    background-color: rgb(240, 240, 255);
    border-right: 2px solid rgb(73, 69, 255);
  }
`

export default Sidebar
