/**
 *  Components - Organisms - Layouts
 */

// Style
import styled from 'styled-components'

// UI
import Sidebar from '../sidebar'

interface LayoutProps {
  children: JSX.Element | JSX.Element[]
}

const handleLogout = (): void => {
  localStorage.removeItem('currentUser')
}

const data = [
  { title: 'Home Page', href: '/home' },
  { title: 'Profile', href: '/profile' },
  { title: 'log out', href: '/login', onClick: (): void => handleLogout() }
]
const Layout = ({ children }: LayoutProps): JSX.Element => {
  return (
    <Body>
      <Sidebar data={data} />
      <LeftLayout>{children}</LeftLayout>
    </Body>
  )
}

const Body = styled.div`
  width: 100%;
  height: 100vh;
  min-height: 100%;
  display: flex;
`

const LeftLayout = styled.div`
  width: 100%;
  padding: 16px 60px;
`
export default Layout
