/**
 * Theme
 */
import COLOR from '../constants/color'
import HEADING from '../constants/headings'
import GRID from '../constants/grid'
const theme = {
  COLOR,
  HEADING,
  GRID
}

export default theme
