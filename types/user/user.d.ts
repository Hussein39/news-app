/**
 * Types - User - IUser
 */

export interface IUser {
  email: string
  firstName: string
  lastName: string
  password: string
  userType: string
}
