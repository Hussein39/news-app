/**
 * Types - Common - IHomePage
 */

import { IBoardAPI } from '../news/board'
import { IBoardNews } from '../news/news'

export interface IHomePage {
  boards: IBoardAPI
  boardNews: IBoardNewsAPI
}

export interface IBoardNewsAPI {
  data: IBoardNews[]
}
