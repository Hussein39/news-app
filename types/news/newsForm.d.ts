import { IBoardAPI } from './board'
import { INews } from './news'

export interface INewsForm {
  boards: IBoardAPI
  isView: boolean
  isUpdate: boolean
  initialValues: INews
  onRefresh: () => void
}
