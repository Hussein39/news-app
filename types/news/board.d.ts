/**
 * Types - Common - IBoard
 */

import { IBoardNews } from './news'

export interface IBoard {
  id: string
  name: string
}

export interface IBoardAPI {
  data: IBoard[] | IBoardNews[]
}
