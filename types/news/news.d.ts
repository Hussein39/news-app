/**
 * Types - Common - ISelect
 */

export interface INews {
  id: string
  boardId: string
  author: string
  title: string
  description: string
  imageURL: string
  createdAt: string
  status: 'draft' | 'published' | 'archive'
}

export interface IBoardNews {
  drafts: INews[]
  published: INews[]
  archives: INews[]
}
