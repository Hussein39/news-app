/**
 * Types - Common - INewsTable
 */

import { IBoardNews, INews } from './news'

export interface INewsTable {
  boardNewsAdapter: IBoardNews[]
  boards: IBoardAPI
  handleChange: (id: string) => void
  handleRemove: (data: INews) => void
  handleUpdate: (data: INews) => void
  handleView: (data: INews) => void
  handleRefresh?: (data?: INews) => void
  initialValues: INews
  isView: boolean
  isShowModal: boolean
  setShowModal: Dispatch<SetStateAction<boolean>>
}

export interface IDialog {
  visible: boolean
  message: string
  title: string
}

export interface INewsTableItem {
  dialog: IDialog
  handleStatus: (status: boolean) => void
  setDialog: Dispatch<SetStateAction<IDialog>>
}
