/**
 * Types - Common - IHeading
 */

export interface IHeading {
  content?: string
  context?: string
  tag: string
}
