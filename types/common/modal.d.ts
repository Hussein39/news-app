/**
 * Types - Common - IModal
 */

export interface IModal {
  children?: JSX.Element
  isRounded?: boolean
  isQuestion?: boolean
  message?: string
  onStatus?: (e: boolean) => void
  setShowModal: Dispatch<SetStateAction<Element[]>>
  title: string
  titleContext?: string
}
