/**
 * Types - Common - ITheme
 */
// Style
import { FlattenSimpleInterpolation } from 'styled-components'

type COLOR = {
  danger: string
  black?: string
  white?: string
}

export interface ITheme {
  theme?: {
    COLOR?: COLOR
  }
}

type CSSReturnType = false | FlattenSimpleInterpolation | undefined | string
