/**
 * Types - Common - ITable
 */

import { IBoardNews, INews } from '../news/news'

export type Column = {
  hidden?: boolean
  key: string
  label?: string
  formatter?: (de: INews) => JSX.Element
}

export interface ITable {
  dataSource?: IBoardNews[]
  column: Column[]
}
