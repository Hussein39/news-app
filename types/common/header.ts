/**
 * Types - Common - IHeader
 */

import { IBoardAPI } from '../news/board'

export interface IHeader {
  boards?: IBoardAPI
  handleRefresh?: () => void
  searchResult: number
}
