/**
 * Types - Common - ISelect
 */

export interface ISelect {
  arrowBorderContext: string
  arrowContext: string
  arrowHoverContext: string
  borderContext: string
  context: string
  content: string
  className: string
  disabled: boolean
  defaultValue: string
  isInlineLabel: boolean
  isText: boolean
  label: string
  name: string
  options: option[]
  onChange: (e: string) => void
  textContext: string
}

export type Option = {
  name: string
  id: string
}
