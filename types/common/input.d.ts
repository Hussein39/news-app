/**
 * Types - Common - IInput
 */

export interface IInput {
  autoFocus: boolean
  borderContext: string
  context: string
  className: string
  disabled: boolean
  defaultValue: string
  errorMessage: string
  id: string
  label: string
  name: string
  onChange: (e: React.FormEvent<HTMLInputElement>) => void
  placeholder: string
  textContext: string
  type: string
  value: string
}
