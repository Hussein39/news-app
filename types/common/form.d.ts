/**
 * Types - Common - IForm
 */

import { INews } from '../news/news'

export interface IRole {
  role: string
  message: string
}

export interface ISchema {
  name: string
  roles: IRole[]
  key: string
}

export interface IForm {
  children: JSX.Element[]
  className?: string
  handleSubmit: (arg: T) => void
  onSubmit?: any
  id?: string
  isUpdate?: boolean
  isView?: boolean
  initialValues?: INews
  loading?: boolean
  schema: Partial<ISchema>[]
  status?: Partial<IStatus>
}

export interface IStatus {
  error: boolean
  message: string
  status: number
  loading: boolean
}
export interface IValidator {
  e
  formElements: JSX.Element[]
  setFormElementProps: Dispatch<SetStateAction<Element[]>>
  schema: Partial<ISchema>[]
}

export interface IValidatorReturn {
  isValid: boolean
  values: Partial<IForm>
}
