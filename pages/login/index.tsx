/**
 * Components - Pages - Login
 */

// Next
import type { NextPage } from 'next'
import LoginPage from '../../components/pages/auth'

const Login: NextPage = () => {
  return <LoginPage />
}

export default Login
