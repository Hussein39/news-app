/**
 * Components - Pages - Home
 */

// Next
import type { NextPage } from 'next'

// UI
import View from '../../components/pages/home'

// API
import loadAll from '../../components/service/board/loadAll'
import loadAllBoardNews from '../../components/service/board/loadAllBoardNews'

const Home: NextPage = ({ boards, boardNews }: any) => {
  return <View boards={boards} boardNews={boardNews} />
}

export async function getServerSideProps({ query }: { query: any }): Promise<any> {
  const boards = await loadAll()
  const boardNews = await loadAllBoardNews(query?.board || 'en')

  return {
    props: { boards, boardNews } // will be passed to the page component as props
  }
}

export default Home
