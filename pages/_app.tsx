/**
 * Pages - App
 */

// React
import { ReactNode } from 'react'

// Next
import type { AppProps } from 'next/app'

// UI
import Guard from '../components/organisms/guard/guard'

// Style
import { ThemeProvider } from 'styled-components'
import theme from '../theme/theme'
import '../styles/globals.css'

function MyApp({ Component, pageProps }: AppProps): ReactNode {
  return (
    <ThemeProvider theme={theme}>
      <Guard>
        <Component {...pageProps} />
      </Guard>
    </ThemeProvider>
  )
}

export default MyApp
