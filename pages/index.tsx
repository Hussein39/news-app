/**
 * Pages - Home
 */

// Next
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect } from 'react'

// UI

const Home: NextPage = () => {
  const { push } = useRouter()

  useEffect(() => {
    if (localStorage.getItem('currentUser')) {
      push('/home')
    } else {
      push('/login')
    }
  }, [])

  return <></>
}

export default Home
